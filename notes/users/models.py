from django.db import models
import datetime

# Create your models here.
class User(models.Model):
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    email = models.EmailField()
    password = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    
    def __str__(self):
        return self.first_name + " " + self.last_name