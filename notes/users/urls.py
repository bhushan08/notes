from django.urls import path
from .viewset.userViewsets import UserViewset

app_name = 'users'

urlpatterns = [
    path('', UserViewset.as_view({'get': 'list'}), name='users')
]