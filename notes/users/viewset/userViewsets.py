from users.models import User
from rest_framework import viewsets
from users.serializer.usersSerializer import UsersSerializer

class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-created_date')
    serializer_class = UsersSerializer