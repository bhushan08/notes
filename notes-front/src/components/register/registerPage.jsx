import React, { Component } from "react";
import "./registerPage.css";

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      password: "",
      email: "",
      confirm_password: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = event => {
    const value = event.currentTarget.value;

    this.setState({
      [event.currentTarget.name]: value
    });
  };

  handleSubmit(event) {
    console.log(this.state);
  }

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <form className="box-body">
            <div className="full-name">
              <div className="input-group first-name">
                <input
                  type="text"
                  id="fname"
                  name="first_name"
                  className="input-area"
                  value={this.state.first_name}
                  onChange={event => this.handleChange(event)}
                  required
                />
                <span htmlFor="inputField" className="label">
                  First Name
                </span>
              </div>

              <div className="input-group last-name">
                <input
                  type="text"
                  id="lname"
                  name="last_name"
                  className="input-area"
                  value={this.state.last_name}
                  onChange={this.handleChange.bind(this)}
                  required
                />
                <span htmlFor="inputField" className="label">
                  Last Name
                </span>
              </div>
            </div>
            <br />

            <div className="full-name email-div">
              <div className="input-group">
                <input
                  type="text"
                  id="email"
                  className="input-area email"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange.bind(this)}
                  required
                />
                <span htmlFor="inputField" className="label">
                  Email
                </span>
              </div>
            </div>
            <br />

            <div className="full-name">
              <div className="input-group first-name">
                <input
                  type="password"
                  id="password"
                  className="input-area"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange.bind(this)}
                  required
                />
                <span htmlFor="inputField" className="label">
                  Password
                </span>
              </div>

              <div className="input-group last-name">
                <input
                  type="password"
                  id="cpassword"
                  className="input-area"
                  name="confirm_password"
                  value={this.state.confirm_password}
                  onChange={this.handleChange.bind(this)}
                  required
                />
                <span htmlFor="inputField" className="label">
                  Confirm
                </span>
              </div>
            </div>
            <br />

            <div className="footer">
              <div className="sign-in-btn">
                <span className="sign-in-btn-span">Sign in instead</span>
              </div>
              <div
                className="blue-btn"
                value="Submit"
                onClick={this.handleSubmit}
              >
                <span className="blue-btn-span">Create</span>
              </div>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default RegisterScreen;
